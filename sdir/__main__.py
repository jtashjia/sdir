#sDir by Jonah Tashjian

#imports:
import os
import re
import sys
import ldap

#main() - main function
def main():

	#variable to store modifiers that user inputs
	args = sys.argv[1:-1]
	#stores query
	query = sys.argv[-1]

	get_info(query, args)

#get_info(query, args): will fetch info from stedwards ldap server
def get_info(query, args):

	#connection to ldap
	ld = ldap.initialize("ldaps://ldapext.stedwards.edu:636")

	ldap_base = "dc=stedwards,dc=edu"

	#if the users inputs student modifier
	if ("-s" in args):

		#username search
		if ("-u" in args):

			query1 = "(&(uid="+query+")(memberOf=Students))"
			result = ld.search_s(ldap_base, ldap.SCOPE_SUBTREE, query1)

		#name search
		elif ("-n" in args):

			query1 = "(&(|(givenName="+query+")(sn="+query+"))(memberOf=Students))"
			result = ld.search_s(ldap_base, ldap.SCOPE_SUBTREE, query1)

		#phone extension search
		elif ("-p" in args):

			print ("\n*cannot query students by phone number*")
			result = []

		#output help
		else:

			print ("\nhelp:")
			print ("[-s] search for student (without this tag, will search for faculty/staff)")
			print ("[-u] search by username")
			print ("[-n] search by name")
			print ("[-p] search by phone ext (only available for faculty/staff)\n")

			result = []

		#output number of results
		print ("\n" + str(len(result)) + " result(s)\n")

		if result:

			#loop through results
			for each in result:

				#set info
				name = each[1]["displayName"][0].decode("utf-8")
				email = each[1]["eduPersonPrincipalName"][0].decode("utf-8")

				#output info
				print (name + "\n" + email + "\n")

	#faculty/staff search, not going to comment because very similar to student search
	else:

		if ("-u" in args):

			query1 = "(&(uid="+query+")(memberOf=Employees))"
			result = ld.search_s(ldap_base, ldap.SCOPE_SUBTREE, query1)

		elif ("-n" in args):

			query1 = "(&(|(givenName="+query+")(sn="+query+"))(memberOf=Employees))"
			result = ld.search_s(ldap_base, ldap.SCOPE_SUBTREE, query1)

		elif ("-p" in args):

			query1 = "(&(telephoneNumber=*"+query+"*)(memberOf=Employees))"
			result = ld.search_s(ldap_base, ldap.SCOPE_SUBTREE, query1)

		elif ("-d" in args):

			query1 = "(&(department=*"+query+"*)(memberOf=Employees))"
			result = ld.search_s(ldap_base, ldap.SCOPE_SUBTREE, query1)

		else:

			print ("\nhelp:")
			print ("[-s] search for student (without this tag, will search for faculty/staff)")
			print ("[-u] search by username")
			print ("[-n] search by name")
			print ("[-p] search by phone ext (only available for faculty/staff)\n")

			result = []

		print ("\n" + str(len(result)) + " result(s)\n")

		if result:

			print (result)

			for each in result:

				name = each[1]["displayName"][0].decode("utf-8")
				email = each[1]["eduPersonPrincipalName"][0].decode("utf-8")
				phone = each[1]["telephoneNumber"][0].decode("utf-8")
				office = each[1]["buildingName"][0].decode("utf-8") + " " + each[1]["roomNumber"][0].decode("utf-8")
				department = each[1]["department"][0].decode("utf-8")
				title = each[1]["title"][0].decode("utf-8")

				print (name + "\n" + email + "\n" + title + " (" + department + ")\n" + phone + "\n" + office + "\n")
