from setuptools import setup

setup(name='py3-sdir',
      version='1.0',
      description='',
      url='http://bitbucket.org/jtashjia/sdir/',
      author='Jonah Tashjian',
      author_email='tashjian.jonah@gmail.com',
      license='MIT',
      packages=['sdir'],
      entry_points = {
        'console_scripts': ['sdir=sdir.__main__:main'],
      },
      install_requires=[
          'python-ldap',
      ],
      zip_safe=False)
