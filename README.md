## sDir

CLI tool to query the St. Edward's University public directory.

**Installation**

git clone https://bitbucket.org/jtashjia/sdir.git

cd sdir

rm -rf .git

pip3 install --user . (<-- don't forget the space and the period)

Done!

**Usage**

sdir [-s] [-u,-n,-p] [query]

-s search for student (without this tag, will search for faculty/staff)

-u search by username

-n search by name

-p search by phone ext (only available for faculty/staff)
